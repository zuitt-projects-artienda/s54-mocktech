let collection = [];

// Write the queue functions below.

// Output all the elements of the queue
function print() {
    return collection;
}
// console.log(collection)
// console.log("HI")

// Adds element to the rear of the queue
function enqueue(element) {
    collection.push(element);
    return collection;
}
//console.log(enqueue("John"))

// Removes element from the front of the queue
//let collection = ["John", "Mary"];
function dequeue() {
  collection.shift();
  return collection;
}
//console.log(dequeue())

// Show element at the front
//let collection = ["John", "Mary"];
function front() {
    return(collection[0])
}
//console.log(front())

// Show total number of elements
//let collection = ["John", "Mary"];
function size() {
return(collection.length)
}
//console.log(size())


// Outputs a Boolean value describing whether queue is empty or not
//let collection = ["John"];
function isEmpty() {
    return (collection.length === 0)
    
}
//console.log(isEmpty())

module.exports = {
    collection,
    print,
    enqueue,
    dequeue,
    front,
    size,
    isEmpty
};